//
//  CardTableViewCell.swift
//  test-bankapp-ios-endcom
//
//  Created by Arturo González on 30/09/21.
//

import UIKit

class CardTableViewCell: UITableViewCell {

    @IBOutlet weak var cardStatus: UILabel!
    
    @IBOutlet weak var cardNumber: UILabel!
    
    @IBOutlet weak var cardName: UILabel!
    @IBOutlet weak var cardAmount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
