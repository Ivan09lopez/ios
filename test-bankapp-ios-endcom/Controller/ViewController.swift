//
//  ViewController.swift
//  test-bankapp-ios-endcom
//
//  Created by Arturo González on 29/09/21.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    @IBOutlet weak var cardsTableView: UITableView!
    
    private let arrayNames = ["Jose", "Juan", "Ernesto"]
    
    private let arrayStatus = ["Activa", "Desactiva", "Activa"]
    
    private let arrayNumber = ["1234 1234 1234 1234", "4321 4321 4321 4321", "1122 1122 1122 1122"]
    
    private let arrayAmount = ["$1200", "$2300", "$1000"]
    
    @IBOutlet weak var movimientosTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       /* movimientosTableView.dataSource = self
        movimientosTableView.delegate = self
        
        movimientosTableView.register(UINib(nibName: "Custom2TableViewCell", bundle: nil), forCellReuseIdentifier: "customCell2")*/
        
        cardsTableView.dataSource = self
        cardsTableView.delegate = self
        
        cardsTableView.register(UINib(nibName: "CustomTableViewCell", bundle: nil), forCellReuseIdentifier: "customCell")
        
        self.cardsTableView.rowHeight = 100
        // Do any additional setup after loading the view.
    }
    
    
   /* func getCards(){
        let url = "http://bankapp.endcom.mx/api/bankappTest/tarjetas"
        AF.request(url, method: .get).validate().validate(statusCode: 200...209).responseDecodable(of: dataResponse){
            response in
            
           
        }
    }*/
    
}

extension ViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*let cell = UITableViewCell(style: .default, reuseIdentifier: "cardsCell")*/
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "customCell", for: indexPath) as? CustomTableViewCell
        
        cell?.txtName!.text = arrayNames[indexPath.row]
        
        cell?.txtStatus!.text = arrayStatus[indexPath.row]
        
        cell?.txtNumber!.text = arrayNumber[indexPath.row]
        
        cell?.txtSaldo!.text = arrayAmount[indexPath.row]
    
        return cell!
    }
    
}

extension ViewController:UITableViewDelegate{
    
}


