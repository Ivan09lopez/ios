//
//  CardDataResponse.swift
//  test-bankapp-ios-endcom
//
//  Created by Arturo González on 30/09/21.
//

import Foundation


struct dataResponse {
    let tarjetas:Tarjetas
}

struct Tarjetas{
    let tarjeta:String
    let nombre:String
    let saldo:Int
    let estado:String
    let tipo:String
    let id:Int
}

/*
 "tarjeta": "5439240112341234",
             "nombre": "Berenice Garcia Lopez",
             "saldo": 1500,
             "estado": "activa",
             "tipo": "titular",
             "id": 2
 
 */
