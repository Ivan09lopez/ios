//
//  CustomTableViewCell.swift
//  test-bankapp-ios-endcom
//
//  Created by Arturo González on 30/09/21.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txtStatus: UILabel!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtNumber: UILabel!
    
    @IBOutlet weak var txtSaldo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
