//
//  Custom2TableViewCell.swift
//  test-bankapp-ios-endcom
//
//  Created by Arturo González on 01/10/21.
//

import UIKit

class Custom2TableViewCell: UITableViewCell {

    @IBOutlet weak var txtAmount: UILabel!
    @IBOutlet weak var txtConcepto: UILabel!
    @IBOutlet weak var txtFecha: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
